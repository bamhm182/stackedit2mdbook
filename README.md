# StackEdit2mdBook

I love StackEdit because it's an OS-agnostic tool that can be used to edit markdown files.
My only complaint is that if you don't want to upload your workspace to the internet, it really sucks for downloading your files.
Additionally, there's no way to search globally within your workspace.

I believe mdBook is by far the best option for converting markdown files into html, but if you're switching between OS's and workstations at random, editing markdown files sucks.
Do you use vim, do you use Notepad++, are neither available to you and you're stuck with Notepad?
StackEdit makes this easy, so long as you're connected to the internet.

To get the best of both worlds, I've created this python script that converts an exported StackEdit workstation into an mdBook.

You can find the example mdBook built from the `StackEdit workspace.json` example in this repo here:

[StackEdit2mdBook](https://bamhm182.gitlab.io/stackedit2mdbook)

## Usage

Create an mdBook folder (`mdbook init`)
Put `se2mdbook.py` in that folder
Export your StackEdit Workspace and put it next to the Python script
Run the Python Script

```
cd $(mktemp -d)
mdbook init
wget https://gitlab.com/bamhm182/stackedit2mdbook/-/raw/master/se2mdbook.py -O se2mdbook.py
cp "~/Downloads/StackEdit workspace.json" ./
./se2mdbook.py
```

Note that since this script rebuilds the src folder, you don't actually even need it in your repo.
Additionally, any changes to the `src` folder will be deleted, so please don't work in there if you're using this.

It's also worth noting that the name of your workspace json file is a little flexible.
As long as it starts with "StackEdit" (case doesn't matter) and ends with ".json", the newest file in your folder that matches that convention will be used automatically.

## CI/CD

With the right CI/CD file, you can make this push to Pages automatically just by updating the `StackEdit workspace.json` and pushing.

Check out the mdbook for this repository [here](https://bamhm182.gitlab.io/stackedit2mdbook).

## StackEdit Format

I used to have it set up so that it would try let you number folders and files within StackEdit so you could define an order, but that just got overly confusing when you have a bunch of files.
For this reason, I removed code that stripped out the numbers, so it will just go based on the alphabetical sorting of your file names now.
In other words, folders and files should appear in the same order as they appear on StackEdit and with the same names.

There is one non-intuitive thing you must do. For any folders, you must have a file named `-Index` within them.
This will become the page that is visible when you click on a folder.

If you want to use things like mermaid, you need to enable that as an mdBook pre-processor.
You can find more information on that [here](https://github.com/badboy/mdbook-mermaid).


You can either import the provided `StackEdit workspace.json` file to StackEdit, or reference the following image:

![](./example-workspace.png)
