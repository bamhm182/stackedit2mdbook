#!/usr/bin/env python3

import json
import codecs
import os
import re
import shutil
import subprocess
from pathlib import Path
import platform
try:
    from unidecode import unidecode
except ModuleNotFoundError:
    if platform.system() == 'Windows':
        print("Windows mdbooks has issues with non-ASCII UTF-8 characters like smart quotes. If you have an issue complaining about UTF-8 characters, either install unidecode with `pip3 install unidecode` or replace any problematic characters within StackEdit")

class StackEdit2mdBook():
    def __init__(self):
        self.ws = dict()

    def read_workspace(self, fp = None):
        if not fp:
            fp = max([x for x in os.listdir() if x.lower().startswith('stackedit') and x.lower().endswith('.json')], key=os.path.getctime)
        with open(fp ,'rb') as f:
            ws = json.load(f)
            for k in ws.keys():
                if ws[k].get('type') in ["file", "folder"] and ws[k].get('parentId') != 'trash' and ws[k].get('name') != None:
                    self.ws[k] = {
                            'name': ws[k].get('name'),
                            'type': ws[k].get('type'),
                            'parentId': ws[k].get('parentId'),
                            'content': ws[f'{k}/content'].get('text') if ws.get(f'{k}/content') else None
                    }

    def make_files(self):
        if os.path.isdir(Path('./src/')):
            shutil.rmtree(Path('./src/'))
        for key in [k for k in self.ws.keys() if self.ws[k].get('type') == 'file']:
            self.ws[key]['path'] = self.get_path(self.ws[key])
            item = self.ws[key]
            os.makedirs(Path("./src/" + re.sub(r"\/?[a-z0-9_]+\.md$", "", item['path'])), exist_ok=True)
            with open(Path(f"./src/{item['path']}"), 'w') as fp:
                try:
                    content = unidecode(item.get('content'))
                except NameError:
                   content = item.get('content') 
                content = re.sub(u'[\u201c\u201d]', '"', content)
                content = re.sub(u'[\u2013\u2014\u2015]', '-', content)
                fp.write(content)

    def make_summary(self):
        summ = [self.ws[k] for k in self.ws.keys() if self.ws[k].get('type') == 'file']
        summ.sort(key= lambda i: (re.sub(r'/[^/]*$', '' , i['path']), i['name']))
        with open(Path('./src/SUMMARY.md'), 'w') as fp:
            fp.write("# SUMMARY\n\n")
            for f in summ:
                if f['name'].lower().endswith('index'):
                    if f.get('parentId'):
                        name = self.ws[f['parentId']]['name']
                    else:
                        with open('book.toml') as config:
                            name = re.findall(r'title\s*=\s*"([^"]*)"', config.read())[0]
                else:
                    name = f['name']
                tabs = f['path'].count("/")
                if f['name'].lower().endswith('index'):
                    tabs -= 1
                line = ("\t" * tabs) + f"- [{name}]({f['path']})\n"
                fp.write(line)

    def get_path(self, item):
        name = item.get('name')
        if item.get('parentId'):
            name = f"{self.get_path(self.ws[item['parentId']])}/{name}"
        if item.get('type') == 'file':
            name = f"{name}.md"
        return self.make_safe_name(name)

    @staticmethod
    def make_safe_name(name):
        name = name.lower()
        name = re.sub(r"-", "", name)
        name = re.sub(r"[^a-z0-9/.]+", "_", name)
        return name

if __name__ == '__main__':
    os.chdir(os.path.dirname(__file__))
    se2m = StackEdit2mdBook()
    se2m.read_workspace()
    se2m.make_files()
    se2m.make_summary()
    build = subprocess.run(['mdbook', 'build'])
    if build.returncode != 0:
        input("\n\nAn Error has occurred...\nPlease read the above messages and press `Enter` to continue...")
